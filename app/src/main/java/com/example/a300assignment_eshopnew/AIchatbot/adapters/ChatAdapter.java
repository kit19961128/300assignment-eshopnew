package com.example.a300assignment_eshopnew.AIchatbot.adapters;

import android.app.Activity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.example.a300assignment_eshopnew.AIchatbot.models.Message;
import com.example.a300assignment_eshopnew.R;
import com.example.a300assignment_eshopnew.SharePreferenceUtil;
import com.example.a300assignment_eshopnew.Sign.User;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;
import com.squareup.picasso.Picasso;

import java.util.List;

import de.hdodenhof.circleimageview.CircleImageView;

public class ChatAdapter extends RecyclerView.Adapter<ChatAdapter.MyViewHolder> {

    private List<Message> messageList;
    private Activity activity;
    private String userID;

    public ChatAdapter(List<Message> messageList, Activity activity) {
        this.messageList = messageList;
        this.activity = activity;
        userID = new SharePreferenceUtil(activity).get("user", "userID", "");
    }

    @NonNull
    @Override
    public MyViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(activity).inflate(R.layout.adapter_message_one, parent, false);
        return new MyViewHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull MyViewHolder holder, int position) {
        String message = messageList.get(position).getMessage();
        boolean isReceived = messageList.get(position).getIsReceived();
        Picasso picasso = Picasso.with(activity);
        if (isReceived) {
            holder.sendLayout.setVisibility(View.GONE);
            holder.receiveLayout.setVisibility(View.VISIBLE);
            holder.messageReceive.setText(message);
            picasso.load(R.drawable.aichatbot)
                    .into(holder.chatbotImage);
        } else {
            holder.receiveLayout.setVisibility(View.GONE);
            holder.sendLayout.setVisibility(View.VISIBLE);
            holder.messageSend.setText(message);
            FirebaseDatabase.getInstance().getReference().child("Users").child(userID).addListenerForSingleValueEvent(new ValueEventListener() {
                @Override
                public void onDataChange(@NonNull DataSnapshot snapshot) {
                    User currentUser = snapshot.getValue(User.class);
                    if (currentUser != null) {
                        picasso.load(currentUser.getImage())
                                .error(R.drawable.profile)
                                .into(holder.userImage);
                    } else {
                        picasso.load(R.drawable.profile)
                                .into(holder.userImage);
                    }
                }

                @Override
                public void onCancelled(@NonNull DatabaseError error) {

                }
            });

        }
    }

    @Override
    public int getItemCount() {
        return messageList.size();
    }

    static class MyViewHolder extends RecyclerView.ViewHolder {

        TextView messageSend;
        TextView messageReceive;
        RelativeLayout sendLayout;
        RelativeLayout receiveLayout;
        ImageView chatbotImage;
        CircleImageView userImage;

        MyViewHolder(@NonNull View itemView) {
            super(itemView);
            messageSend = itemView.findViewById(R.id.message_send);
            messageReceive = itemView.findViewById(R.id.message_receive);
            sendLayout = itemView.findViewById(R.id.sendLayout);
            receiveLayout = itemView.findViewById(R.id.receiveLayout);
            chatbotImage = itemView.findViewById(R.id.chatbotImage);
            userImage = itemView.findViewById(R.id.userImage);
        }
    }

}
