package com.example.a300assignment_eshopnew.ARcore;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.example.a300assignment_eshopnew.R;
import com.squareup.picasso.Picasso;

import java.util.ArrayList;

public class RecyclerViewAdapter extends RecyclerView.Adapter<RecyclerViewAdapter.ViewHolder> {
    private ArrayList<String> ProductNames = new ArrayList<>();
    private ArrayList<String> ImagePath = new ArrayList<>();
    private Context context;
    private ArrayList<String> ModelNames = new ArrayList<>();


    public RecyclerViewAdapter(Context context, ArrayList<String> ProductNames, ArrayList<String> ImagePath, ArrayList<String> ModelNames) {
        this.ProductNames = ProductNames;
        this.ImagePath = ImagePath;
        this.ModelNames = ModelNames;
        this.context = context;
    }

    @NonNull
    @Override
    public ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {

        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.layout_list_item,parent,false);
        return new ViewHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull ViewHolder holder, int position) {
        //Set image
        Picasso.with(context).load(ImagePath.get(position)).into(holder.imageView);
        //Set product name
        holder.textView.setText(ProductNames.get(position));
        //Set OnClick
        holder.imageView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Common.model = ModelNames.get(position);
            }
        });
    }

    @Override
    public int getItemCount() {
        return ImagePath.size();
    }

    public class ViewHolder extends RecyclerView.ViewHolder{

        ImageView imageView;
        TextView textView;

        public ViewHolder(@NonNull View itemView) {
            super(itemView);

            imageView = itemView.findViewById(R.id.imageview);
            textView = itemView.findViewById(R.id.text);
        }
    }

}
