package com.example.a300assignment_eshopnew.Favourite;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.view.View;

import com.example.a300assignment_eshopnew.R;
import com.example.a300assignment_eshopnew.base.BaseAdapter;
import com.example.a300assignment_eshopnew.databinding.FavouriteItemsLayoutBinding;
import com.example.a300assignment_eshopnew.product.Product;
import com.example.a300assignment_eshopnew.product.Productdetail.ProductdetailActivity;
import com.squareup.picasso.Picasso;

import java.io.Serializable;

public class FavouriteAdapter extends BaseAdapter<Product, FavouriteItemsLayoutBinding> {

    protected Context context;

    private FavouriteAdapter() {
    }

    public FavouriteAdapter(Context context) {
        this.context = context;
    }



    @Override
    protected void onBindData(FavouriteItemsLayoutBinding viewBinding, Product Favourite, int position) {


        //Add the Text
        viewBinding.productName.setText(Favourite.getProductName());
        viewBinding.productPrice.setText("$" + Favourite.getProductPrice());
        viewBinding.productRating.setRating(Float.parseFloat(Favourite.getStar()));


        if (Favourite.getAr_modelLink().equals("N")) {
            viewBinding.imageAR.setVisibility(View.INVISIBLE);
        }

        //Add product image
        Picasso.with(context).load(Favourite.getImage()).into(viewBinding.productImage);


        viewBinding.layout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(context, ProductdetailActivity.class);
                intent.putExtra("Product", (Serializable) Favourite);
                context.startActivity(intent);
                ((Activity) context).overridePendingTransition(R.anim.slide_in_right, R.anim.slide_out_left);

            }
        });

    }
}
