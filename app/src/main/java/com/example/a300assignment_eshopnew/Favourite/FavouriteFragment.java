package com.example.a300assignment_eshopnew.Favourite;

import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.LinearLayoutManager;

import com.example.a300assignment_eshopnew.R;
import com.example.a300assignment_eshopnew.SharePreferenceUtil;
import com.example.a300assignment_eshopnew.databinding.ActivityFavouriteFragmentBinding;
import com.example.a300assignment_eshopnew.firebase.FirebaseFavouriteUtil;
import com.example.a300assignment_eshopnew.product.Product;
import com.example.a300assignment_eshopnew.swipe.RecyclerTouchListener;
import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.ValueEventListener;

public class FavouriteFragment extends Fragment implements AdapterView.OnItemSelectedListener {

    private String userID = "";

    private DatabaseReference DB_eference;
    private FavouriteAdapter adapter;


    private SharePreferenceUtil sharePreferenceUtil;

    private ValueEventListener listener;
    ActivityFavouriteFragmentBinding viewbinding;

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        final View view = inflater.inflate(R.layout.activity_favourite_fragment, container, false);
        viewbinding = ActivityFavouriteFragmentBinding.bind(view);

        sharePreferenceUtil = new SharePreferenceUtil(this.requireContext());
        // SharedPreferences cart = getActivity().getPreferences("cart", HomeActivity.MODE_PRIVATE);
        // userID = getSharedPreferences("data",MODE_PRIVATE).getString("Uid","");

        userID = sharePreferenceUtil.get("user", "userID", "");

        FavouriteListRecycler();


        return view;
    }


    private void FavouriteListRecycler() {
        viewbinding.FavouriteListView.setHasFixedSize(true);
        viewbinding.FavouriteListView.setLayoutManager(new LinearLayoutManager(getContext(), LinearLayoutManager.VERTICAL, false));
        adapter = new FavouriteAdapter(getContext());
        viewbinding.FavouriteListView.setAdapter(adapter);
        RecyclerTouchListener touchListener = new RecyclerTouchListener(getActivity(), viewbinding.FavouriteListView);

        viewbinding.FavouriteListView.addOnItemTouchListener(touchListener);
    }

    @Override
    public void onResume() {
        super.onResume();
        //Call firebase
        DB_eference = FirebaseFavouriteUtil.getFavouriteDatabase(userID);
        DB_eference.get().addOnCompleteListener(new OnCompleteListener<DataSnapshot>() {
            @Override
            public void onComplete(@NonNull Task<DataSnapshot> task) {
                if (task.isSuccessful()) {
                    DataSnapshot snapshot = task.getResult();
                    adapter.removeAllData();
                    for (DataSnapshot snapshot1 : snapshot.getChildren()) {
                        Product favourite = snapshot1.getValue(Product.class);
                        adapter.addData(favourite);
                    }

                }
            }
        });
        // listener = new ValueEventListener() {
        //     @Override
        //     public void onDataChange(@NonNull DataSnapshot snapshot) {
        //         adapter.removeAllData();
        //         for (DataSnapshot snapshot1 : snapshot.getChildren()) {
        //             Cart cart = snapshot1.getValue(Cart.class);
        //             adapter.addData(cart);
        //         }
        //         // for (DataSnapshot datas : snapshot.getChildren()) {
        //         //     Cart cart = datas.getValue(Cart.class);
        //         //     cart.setPid(datas.getKey());
        //         //     cartList.add(cart);
        //     }
        //
        //     @Override
        //     public void onCancelled(@NonNull DatabaseError error) {
//
        //     }
        // };
        //Get the firebase data
        //DB_eference.addValueEventListener(listener);

    }


    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
    }


    @Override
    public void onItemSelected(AdapterView<?> adapterView, View view, int i, long l) {


    }

    @Override
    public void onNothingSelected(AdapterView<?> adapterView) {

    }


}