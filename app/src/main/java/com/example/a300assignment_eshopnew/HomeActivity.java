package com.example.a300assignment_eshopnew;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;
import androidx.core.view.GravityCompat;
import androidx.drawerlayout.widget.DrawerLayout;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentManager;
import androidx.fragment.app.FragmentTransaction;

import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.preference.PreferenceManager;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

//import com.example.a300assignment_eshopnew.AIchatbot.aiChatbotActivity;
import com.example.a300assignment_eshopnew.AIchatbot.aiChatbotActivity;
import com.example.a300assignment_eshopnew.Favourite.FavouriteFragment;
import com.example.a300assignment_eshopnew.Profile.ProfileActivity;
import com.example.a300assignment_eshopnew.Sign.LoginActivity;
import com.example.a300assignment_eshopnew.Sign.User;
import com.example.a300assignment_eshopnew.databinding.MenuHeaderBinding;
import com.example.a300assignment_eshopnew.home.homepageFragment;
import com.example.a300assignment_eshopnew.databinding.ActivityHomeBinding;
import com.example.a300assignment_eshopnew.product.productFragment;
import com.example.a300assignment_eshopnew.product.ShoppingCart.CartFragment;
import com.example.a300assignment_eshopnew.utils.ActivityUtil;
import com.google.android.material.navigation.NavigationView;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;
import com.hl3hl3.arcoremeasure.ArMeasureActivity;
import com.squareup.picasso.Picasso;

public class HomeActivity extends AppCompatActivity implements NavigationView.OnNavigationItemSelectedListener {

    static final float END_SCALE = 0.7f;

    private ActivityHomeBinding viewBinding;

    DrawerLayout drawerLayout;
    NavigationView navigationView;

    private DatabaseReference DB_reference;
    private String userID = "";
    //logged header
    private TextView name, verified, header_text;
    private ImageView imageProfile, cart;
    private SharePreferenceUtil sharePreferenceUtil;

    private int test = 0;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        ActivityUtil.activity = this;
        super.onCreate(savedInstanceState);
        sharePreferenceUtil = new SharePreferenceUtil(this);
        viewBinding = ActivityHomeBinding.inflate((LayoutInflater.from(this)));
        setContentView(viewBinding.getRoot());


        drawerLayout = findViewById(R.id.drawer_layout);
        navigationView = findViewById(R.id.navigation_view);
        View headerView = navigationView.getHeaderView(0);

        name = headerView.findViewById(R.id.username);
        header_text = headerView.findViewById(R.id.header_text);
        verified = headerView.findViewById(R.id.verified);

        imageProfile = headerView.findViewById(R.id.imageProfile);


        cart = findViewById(R.id.cart);

        //show menu
        Menu menu = navigationView.getMenu();

        //get userID
        userID = getSharedPreferences("data", MODE_PRIVATE).getString("Uid", "");
        //pass userID to other pages
        sharePreferenceUtil.put("user", "userID", userID);


        if (userID != "") {
            menu.findItem(R.id.nav_login).setVisible(false);
            DB_reference = FirebaseDatabase.getInstance().getReference("Users");

            DB_reference.child(userID).addValueEventListener(new ValueEventListener() {
                @Override
                public void onDataChange(@NonNull DataSnapshot snapshot) {
                    User userProfile = snapshot.getValue(User.class);
                    if (userProfile != null) {
                        String fullname = userProfile.getName();
                        name.setText(fullname);

                        Picasso.with(getApplicationContext()).load(userProfile.getImage())
                                .error(R.drawable.profile)
                                .into(imageProfile);
                    }
                }

                @Override
                public void onCancelled(@NonNull DatabaseError error) {

                }
            });
            header_text.setVisibility(View.INVISIBLE);


        } else {
            imageProfile.setVisibility(View.INVISIBLE);
            name.setVisibility(View.INVISIBLE);
            verified.setVisibility(View.INVISIBLE);
            menu.findItem(R.id.nav_logout).setVisible(false);
            menu.findItem(R.id.nav_profile).setVisible(false);
            menu.findItem(R.id.nav_cart).setVisible(false);
            menu.findItem(R.id.nav_favourite).setVisible(false);
            cart.setVisibility(View.INVISIBLE);
        }


        NaviationDrawer();
        DefaultHome();

    }


    //Navigation Drawer Animation
    private void NavigationDrawerAnimation() {

        drawerLayout.addDrawerListener(new DrawerLayout.SimpleDrawerListener() {
                                           @Override
                                           public void onDrawerSlide(View drawerView, float slideOffset) {
                                               final float diffScaledOffset = slideOffset * (1 - END_SCALE);
                                               final float offsetScale = 1 - diffScaledOffset;
                                               viewBinding.content.setScaleX(offsetScale);
                                               viewBinding.content.setScaleY(offsetScale);

                                               final float xoffset = drawerView.getWidth() * slideOffset;
                                               final float xoffsetDiff = viewBinding.content.getWidth() * diffScaledOffset / 2;
                                               final float xTranslation = xoffset - xoffsetDiff;
                                               viewBinding.content.setTranslationX(xTranslation);
                                           }
                                       }
        );
    }


    //Drawer Functions
    private void NaviationDrawer() {
        //Navigation Drawer

        navigationView.bringToFront();
        navigationView.setNavigationItemSelectedListener(this);
        navigationView.setCheckedItem(R.id.nav_home);

        viewBinding.menuIcon.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (drawerLayout.isDrawerVisible(GravityCompat.START))
                    drawerLayout.closeDrawer(GravityCompat.START);
                else
                    drawerLayout.openDrawer(GravityCompat.START);
            }
        });

        NavigationDrawerAnimation();
    }


    @Override
    public void onBackPressed() {
        if (drawerLayout.isDrawerVisible(GravityCompat.START)) {
            drawerLayout.closeDrawer(GravityCompat.START);
        } else
            super.onBackPressed();
    }


    @Override
    public boolean onNavigationItemSelected(@NonNull MenuItem item) {
        Fragment fragment = null;

        switch (item.getItemId()) {
            case R.id.nav_home:
                fragment = new homepageFragment();
                viewBinding.title.setText("Home");
                break;

            case R.id.nav_product:
                fragment = new productFragment();
                viewBinding.title.setText("Product");
                break;

            case R.id.nav_cart:
                fragment = new CartFragment();
                viewBinding.title.setText("Cart");
                //   SharedPreferences cart = this.getSharedPreferences("data", Context.MODE_PRIVATE);
                //pass userid to fragment
                sharePreferenceUtil.put("user", userID, userID);
                break;

            case R.id.nav_profile:
                Intent intent_profile = new Intent(getApplicationContext(), ProfileActivity.class);
                startActivity(intent_profile);
                break;

            case R.id.nav_AIchatbot:
                Intent intent_AIchatbot = new Intent(getApplicationContext(), aiChatbotActivity.class);
                startActivity(intent_AIchatbot);
                break;

            case R.id.nav_favourite:
                fragment = new FavouriteFragment();
                viewBinding.title.setText("Favourite");
                sharePreferenceUtil.put("user", userID, userID);
                break;

            case R.id.nav_ARmeasure:
                Intent intent_ARmeasure = new Intent(getApplicationContext(), ArMeasureActivity.class);
                startActivity(intent_ARmeasure);
                break;

            case R.id.nav_login:
                Intent intent_login = new Intent(getApplicationContext(), LoginActivity.class);
                startActivity(intent_login);
                break;

            case R.id.nav_logout:
                SharedPreferences settings = this.getSharedPreferences("data", Context.MODE_PRIVATE);
                settings.edit().remove("Uid").commit();
                FirebaseAuth.getInstance().signOut();
                Intent intent = new Intent(this, HomeActivity.class);
                intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK | Intent.FLAG_ACTIVITY_NEW_TASK);
                startActivity(intent);
                overridePendingTransition(R.anim.slide_in_right, R.anim.slide_out_left);
                Toast.makeText(this, "Logout successfully", Toast.LENGTH_LONG).show();
                break;

        }

        if (fragment != null) {
            FragmentManager fragmentManager = getSupportFragmentManager();
            fragmentManager.beginTransaction().replace(R.id.flContent, fragment).commit();
        }

        drawerLayout.closeDrawer(GravityCompat.START);
        return true;


    }

    //
    public void selectNavigationViewItem(MenuItem item) {
        try {
            navigationView.setCheckedItem(item);
            onNavigationItemSelected(item);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }


    private void DefaultHome() {
        Fragment fragment;
        FragmentManager fragmentManager = getSupportFragmentManager();
        FragmentTransaction ft = fragmentManager.beginTransaction();
        fragment = new homepageFragment();
        ft.replace(R.id.flContent, fragment);
        ft.commit();

    }

    //cart icon and goto cart fragment
    public void cart(View view) {
        Fragment fragment1 = null;
        fragment1 = new CartFragment();
        viewBinding.title.setText("Cart");
        sharePreferenceUtil.put("user", userID, userID);

        if (fragment1 != null) {
            FragmentManager fragmentManager = getSupportFragmentManager();
            fragmentManager.beginTransaction().replace(R.id.flContent, fragment1).commit();
        }
    }

}