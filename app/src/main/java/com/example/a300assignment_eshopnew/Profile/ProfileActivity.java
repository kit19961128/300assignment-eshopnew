package com.example.a300assignment_eshopnew.Profile;

import android.annotation.SuppressLint;
import android.app.ProgressDialog;
import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;

import com.example.a300assignment_eshopnew.R;
import com.example.a300assignment_eshopnew.Sign.User;
import com.example.a300assignment_eshopnew.databinding.ActivityProfileBinding;
import com.example.a300assignment_eshopnew.utils.ActivityUtil;
import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;
import com.google.android.material.textfield.TextInputEditText;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;
import com.google.firebase.storage.FirebaseStorage;
import com.google.firebase.storage.StorageReference;
import com.google.firebase.storage.StorageTask;
import com.google.firebase.storage.UploadTask;
import com.squareup.picasso.Picasso;
import com.theartofdev.edmodo.cropper.CropImage;

import java.net.URI;

public class ProfileActivity extends AppCompatActivity {

    FirebaseAuth mAuth;
    FirebaseUser firebaseUser;
    private URI imageUri;
    private String myURI;
    StorageTask uploadTask;
    StorageReference storageProfileReference;
    DatabaseReference databaseReference;
    FirebaseDatabase mFirebaseDatabase;
    private ImageView back_icon, profile_pic;
    private Button btn_update;

    private TextView ResetPassword,test;
    private TextInputEditText et_email, et_username, et_phone;

    private String userID, email, name, image;


    ActivityProfileBinding viewBinding;

    private User currentUser;

    private Uri profilePicUri;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_profile);

        Intent intent = getIntent();
        String name = intent.getStringExtra("name");

        ResetPassword = findViewById(R.id.ResetPassword);
        profile_pic = findViewById(R.id.profile_pic);
        btn_update = findViewById(R.id.btn_update);
        back_icon = findViewById(R.id.back_icon);
        back_icon.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
            }
        });

        // Hooks
        et_email = findViewById(R.id.profile_email);
        et_phone = findViewById(R.id.profile_phone);
        et_username = findViewById(R.id.profile_username);


        mAuth = FirebaseAuth.getInstance();
        firebaseUser = mAuth.getCurrentUser();

        userID = firebaseUser.getUid();

        //get default firebasedatabase instance
        mFirebaseDatabase = FirebaseDatabase.getInstance();

        //getdata and pass to edit text
        databaseReference = mFirebaseDatabase.getReference().child("Users");
        databaseReference.child(userID).addValueEventListener(new ValueEventListener() {
            @Override
            public void onDataChange(@NonNull DataSnapshot dataSnapshot) {
                currentUser = dataSnapshot.getValue(User.class);

                et_username.setText(currentUser.getName());
                et_phone.setText(currentUser.getPhone());
                et_email.setText(firebaseUser.getEmail());
                Picasso.with(getApplicationContext()).load(currentUser.getImage())
                        .error(R.drawable.profile)
                        .into(profile_pic);
                if (currentUser.getNormalUser().equals("true")) {

                    ResetPassword.setVisibility(View.VISIBLE);
                } else {

                    ResetPassword.setVisibility(View.GONE);
                }
            }


            @Override
            public void onCancelled(@NonNull DatabaseError databaseError) {

            }
        });


        // backIcon.setOnClickListener(this);
        //  btn_update.setOnClickListener(this);
        //  profile_pic .setOnClickListener(this);

        //function crop photo
        profile_pic.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                CropImage.activity().setAspectRatio(1, 1).start(com.example.a300assignment_eshopnew.Profile.ProfileActivity.this);
            }
        });

        // getUserinfo();

        onClickUpdate();
        onClickResetPassword();

    }

    private void onClickResetPassword() {
        ResetPassword.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View v) {
                ResetPassword();
            }
        });
    }

    private void ResetPassword() {
        String email = firebaseUser.getEmail();

        //call the Api forget password
        mAuth.sendPasswordResetEmail(email).addOnCompleteListener(new OnCompleteListener<Void>() {
            @Override
            public void onComplete(@NonNull Task<Void> task) {
                if (task.isSuccessful()) {
                    Toast.makeText(getApplicationContext(), "Check your email to reset your password!", Toast.LENGTH_LONG).show();
                    finish();
                } else {
                    Toast.makeText(getApplicationContext(), "Try again! Something wrong happened!", Toast.LENGTH_LONG).show();
                }
            }
        });

    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (requestCode == CropImage.CROP_IMAGE_ACTIVITY_REQUEST_CODE) {
            CropImage.ActivityResult result = CropImage.getActivityResult(data);
            if (resultCode == RESULT_OK) {
                profilePicUri = result.getUri();
                Picasso.with(getApplicationContext()).load(profilePicUri).into(profile_pic);

                //profile_pic.setImageURI(resultUri);
            } else if (resultCode == CropImage.CROP_IMAGE_ACTIVITY_RESULT_ERROR_CODE) {
                Exception error = result.getError();
            }
        }
    }

    public void onClickUpdate() {
        btn_update.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                updatedata();
            }
        });

    }

    private void updatedata() {


        if (et_username.getEditableText().toString().isEmpty()) {
            Toast.makeText(this, "Name cannot empty", Toast.LENGTH_LONG).show();
        } else if (et_phone.getEditableText().toString().isEmpty()) {
            Toast.makeText(this, "Phone number cannot empty", Toast.LENGTH_LONG).show();
        } else {
            currentUser.setName(et_username.getEditableText().toString())
                    .setPhone(et_phone.getEditableText().toString());
            if (profilePicUri != null) {
                //prevent activity finished before dialog dismiss
                ProgressDialog progressDialog = new ProgressDialog(ActivityUtil.activity); //call activity as context
                progressDialog.setTitle("Update Profile");
                progressDialog.setMessage("Please wait, we updating your profile");
                progressDialog.show();


                StorageReference storageReference = FirebaseStorage.getInstance().getReference(userID);
                storageReference.putFile(profilePicUri).addOnCompleteListener(new OnCompleteListener<UploadTask.TaskSnapshot>() {
                    @Override
                    public void onComplete(@NonNull Task<UploadTask.TaskSnapshot> task) {
                        if (task.isSuccessful()) {
                            storageReference.getDownloadUrl().addOnCompleteListener(new OnCompleteListener<Uri>() {
                                @Override
                                public void onComplete(@NonNull Task<Uri> task) {
                                    progressDialog.dismiss();
                                    currentUser.setImage(task.getResult().toString());
                                    databaseReference.child(userID).setValue(currentUser).addOnCompleteListener(new OnCompleteListener<Void>() {
                                        @Override
                                        public void onComplete(@NonNull Task<Void> task) {
                                            Toast.makeText(com.example.a300assignment_eshopnew.Profile.ProfileActivity.this, "Data has been updated", Toast.LENGTH_LONG).show();
                                            finish();
                                        }
                                    });
                                }
                            });
                        } else {
                            Toast.makeText(com.example.a300assignment_eshopnew.Profile.ProfileActivity.this, "Profile image upload fail", Toast.LENGTH_SHORT).show();
                            progressDialog.dismiss();
                            databaseReference.child(userID).setValue(currentUser).addOnCompleteListener(new OnCompleteListener<Void>() {
                                @Override
                                public void onComplete(@NonNull Task<Void> task) {
                                    Toast.makeText(com.example.a300assignment_eshopnew.Profile.ProfileActivity.this, "Data update failed", Toast.LENGTH_LONG).show();
                                    finish();
                                }
                            });
                        }
                    }
                });
            } else {
                databaseReference.child(userID).setValue(currentUser).addOnCompleteListener(new OnCompleteListener<Void>() {
                    @Override
                    public void onComplete(@NonNull Task<Void> task) {
                        Toast.makeText(com.example.a300assignment_eshopnew.Profile.ProfileActivity.this, "Data has been updated", Toast.LENGTH_LONG).show();
                        finish();
                    }
                });
            }
        }


    }



   /* private void getUserinfo() {
        databaseReference.child(mAuth.getCurrentUser().getUid()).addValueEventListener(new ValueEventListener() {
            @Override
            public void onDataChange(DataSnapshot dataSnapshot) {

                        String name = dataSnapshot.child("name").getValue().toString();
                        //name = et_username;
                        String phone = dataSnapshot.child("phone").getValue().toString();
                        et_username.setText(name);
                        et_phone.setText(phone);

                        if (dataSnapshot.hasChild("image")) {
                            String image = dataSnapshot.child("image").getValue().toString();
                            Picasso.get().load(image).into(profile_pic);

                    }
            }

            @Override
            public void onCancelled(DatabaseError databaseError) {

            }
        });
    }


    private void validateAndsave() {
        if (et_username.isEmpty())
        {
            Toast.makeText(this, "Please Enter Your Name", Toast.LENGTH_SHORT).show();
        }else if (TextUtils.isEmpty(edtPhone.getText().toString()))
        {
            Toast.makeText(this, "Please Enter Your Phone No. ", Toast.LENGTH_SHORT).show();
        }else {
            HashMap<String,Object> userMap = new HashMap<>();
            userMap.put("name",edtName.getText().toString());
            userMap.put("phone",edtPhone.getText().toString());

            databaseReference.child(mAuth.getCurrentUser().getUid()).updateChildren(userMap);

            uploadProfileImage();
        }
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, @Nullable Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (requestCode  == CropImage.CROP_IMAGE_ACTIVITY_REQUEST_CODE && resultCode == RESULT_OK && data != null)
        {
            CropImage.ActivityResult result = CropImage.getActivityResult(data);
            imageUri = result.getUri();

            profile_pic.setImageURI(imageUri);
        }
        else {
            Toast.makeText(this, "Error, Try again", Toast.LENGTH_SHORT).show();
        }
    }





    private void uploadProfileImage() {

        final ProgressDialog progressDialog = new ProgressDialog(this);
        progressDialog.setTitle("Set your profile");
        progressDialog.setMessage("Please wait, while we are setting your data ");
        progressDialog.show();

        if (imageUri != null)
        {
            final StorageReference fileRef = storageProfilePicsRef
                    .child(mAuth.getCurrentUser().getUid()+ ".jpg");

            uploadTask = fileRef.putFile(imageUri);


            uploadTask.continueWithTask(new Continuation() {
                @Override
                public Object then(@NonNull Task task) throws Exception {
                    if (!task.isSuccessful())
                    {
                        throw task.getException();
                    }
                    return fileRef.getDownloadUrl();
                }
            }).addOnCompleteListener(new OnCompleteListener<Uri>() {
                @Override
                public void onComplete(@NonNull Task<Uri> task) {
                    if (task.isSuccessful())
                    {
                        Uri downloadUrl =task.getResult();
                        myUri = downloadUrl.toString();

                        HashMap<String, Object> userMap = new HashMap<>();
                        userMap.put("image",myUri);

                        databaseReference.child(mAuth.getCurrentUser().getUid()).updateChildren(userMap);

                        progressDialog.dismiss();


                    }

                }
            });
        }
        else {
            progressDialog.dismiss();
            Toast.makeText(this, "Data update", Toast.LENGTH_SHORT).show();
        }

    }*/


}