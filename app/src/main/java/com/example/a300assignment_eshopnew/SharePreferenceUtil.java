package com.example.a300assignment_eshopnew;

import android.content.Context;
import android.content.SharedPreferences;

public class SharePreferenceUtil {

    public SharePreferenceUtil(Context context) {
        this.context = context;
    }

    private final Context context;

    public void put(String name, String key, String value) {
        SharedPreferences sharedPrefs = context.getSharedPreferences(name, Context.MODE_PRIVATE);
        SharedPreferences.Editor editor = sharedPrefs.edit();
        editor.putString(key, value);
        editor.apply();

    }

    public String get(String name, String key, String defValue) {
        SharedPreferences sharedPrefs = context.getSharedPreferences(name, Context.MODE_PRIVATE);
        return sharedPrefs.getString(key, defValue);
    }
}
