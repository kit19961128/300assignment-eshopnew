package com.example.a300assignment_eshopnew.Sign;


import android.app.Activity;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.util.Patterns;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;

import com.example.a300assignment_eshopnew.HomeActivity;
import com.example.a300assignment_eshopnew.R;
import com.example.a300assignment_eshopnew.databinding.LoginTabFragmentBinding;
import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.auth.AuthResult;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;

import static android.content.Context.MODE_PRIVATE;


public class LoginTabFragment extends Fragment implements View.OnClickListener {

     private FirebaseAuth mAuth;
     private LoginTabFragmentBinding viewBinding;
     float v;

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        ViewGroup root = (ViewGroup) inflater.inflate(R.layout.login_tab_fragment,container,false);
        viewBinding = LoginTabFragmentBinding.bind(root);


        mAuth = FirebaseAuth.getInstance();

        viewBinding.etEmail.setTranslationX(800);
        viewBinding.etPassword.setTranslationX(800);
        viewBinding.buttonForgotPassword.setTranslationX(800);
        viewBinding.buttonSignin.setTranslationX(800);

        viewBinding.etEmail.setAlpha(v);
        viewBinding.etPassword.setAlpha(v);
        viewBinding.buttonForgotPassword.setAlpha(v);
        viewBinding.buttonSignin.setAlpha(v);

        viewBinding.etEmail.animate().translationX(0).alpha(1).setDuration(800).setStartDelay(300).start();
        viewBinding.etPassword.animate().translationX(0).alpha(1).setDuration(800).setStartDelay(500).start();
        viewBinding.buttonForgotPassword.animate().translationX(0).alpha(1).setDuration(800).setStartDelay(500).start();
        viewBinding.buttonSignin.animate().translationX(0).alpha(1).setDuration(800).setStartDelay(700).start();


        viewBinding.buttonSignin.setOnClickListener(this);
        viewBinding.buttonForgotPassword.setOnClickListener(this);

        return root;
    }

    public void onClick(View v) {
        if (v == viewBinding.buttonSignin) {
            Login();
        }else if(v == viewBinding.buttonForgotPassword){
            Forgetpassword();
        }
    }

    private void Forgetpassword() {
        Intent intent = new Intent(getContext(), ForgetpasswordActivity.class);
        getContext().startActivity(intent);
        ((Activity)getContext()).overridePendingTransition(R.anim.slide_in_right,R.anim.slide_out_left);
    }


    private void Login(){

        String ipemail = viewBinding.etEmail.getText().toString().trim();
        String ippassword = viewBinding.etPassword.getText().toString().trim();

        //check email
        if(ipemail.isEmpty()){
            viewBinding.etEmail.setError("Email is required!");
            viewBinding.etEmail.requestFocus();
            return;
        }

        //check email pattern
        if(!Patterns.EMAIL_ADDRESS.matcher(ipemail).matches()){
            viewBinding.etEmail.setError("Please provide valid email!");
            viewBinding.etEmail.requestFocus();
            return;
        }

        //check password
        if(ippassword.isEmpty()){
            viewBinding.etPassword.setError("password is required!");
            viewBinding.etPassword.requestFocus();
            return;
        }

        viewBinding.progressBar.setVisibility(View.VISIBLE);

        mAuth.signInWithEmailAndPassword(ipemail,ippassword).addOnCompleteListener(new OnCompleteListener<AuthResult>() {
            @Override
            public void onComplete(@NonNull Task<AuthResult> task) {
                if(task.isSuccessful()){
                    FirebaseUser user = FirebaseAuth.getInstance().getCurrentUser();


                    if(user.isEmailVerified()) {

                        //Set sharedPreferences
                        SharedPreferences pref = getActivity().getSharedPreferences("data", MODE_PRIVATE);
                        pref.edit().putString("Uid",user.getUid()).commit();

                        Intent intent = new Intent(getContext(), HomeActivity.class);
                        intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK| Intent.FLAG_ACTIVITY_NEW_TASK);
                        getContext().startActivity(intent);
                        Toast.makeText(getContext(),"Login successfully", Toast.LENGTH_LONG).show();

                    }else{
                        user.sendEmailVerification();
                        Toast.makeText(getContext(),"Check your email to verify you account!", Toast.LENGTH_LONG).show();
                        viewBinding.progressBar.setVisibility(View.GONE);
                    }

                }else{
                    Toast.makeText(getContext(),"Failed to login! Please check you credentials", Toast.LENGTH_LONG).show();
                    viewBinding.progressBar.setVisibility(View.GONE);
                }
            }
        });
    }

}
