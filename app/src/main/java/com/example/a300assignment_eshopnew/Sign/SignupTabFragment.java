package com.example.a300assignment_eshopnew.Sign;

import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.util.Patterns;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;

import com.example.a300assignment_eshopnew.HomeActivity;
import com.example.a300assignment_eshopnew.R;
import com.example.a300assignment_eshopnew.databinding.SignupTabFragmentBinding;
import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.auth.AuthResult;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;
import com.google.firebase.database.FirebaseDatabase;

import static android.content.Context.MODE_PRIVATE;

public class SignupTabFragment extends Fragment implements View.OnClickListener {


    private FirebaseAuth mAuth;
    private SignupTabFragmentBinding viewBinding;

    float v;

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        ViewGroup root = (ViewGroup) inflater.inflate(R.layout.signup_tab_fragment, container, false);
        viewBinding = SignupTabFragmentBinding.bind(root);


        viewBinding.etEmail.setTranslationX(800);
        viewBinding.etUsername.setTranslationX(800);
        viewBinding.etPhone.setTranslationX(800);
        viewBinding.etPassword.setTranslationX(800);
        viewBinding.etConfirmPassword.setTranslationX(800);
        viewBinding.buttonMainSignup.setTranslationX(800);

        viewBinding.etEmail.setAlpha(v);
        viewBinding.etUsername.setAlpha(v);
        viewBinding.etPhone.setAlpha(v);
        viewBinding.etPassword.setAlpha(v);
        viewBinding.etConfirmPassword.setAlpha(v);
        viewBinding.buttonMainSignup.setAlpha(v);

        viewBinding.etEmail.animate().translationX(0).alpha(1).setDuration(800).setStartDelay(500).start();
        viewBinding.etUsername.animate().translationX(0).alpha(1).setDuration(800).setStartDelay(500).start();
        viewBinding.etPhone.animate().translationX(0).alpha(1).setDuration(800).setStartDelay(500).start();
        viewBinding.etPassword.animate().translationX(0).alpha(1).setDuration(800).setStartDelay(500).start();
        viewBinding.etConfirmPassword.animate().translationX(0).alpha(1).setDuration(800).setStartDelay(500).start();
        viewBinding.buttonMainSignup.animate().translationX(0).alpha(1).setDuration(800).setStartDelay(500).start();

        viewBinding.buttonMainSignup.setOnClickListener(this);

        mAuth = FirebaseAuth.getInstance();

        return root;
    }


    @Override
    public void onClick(View v) {
        if (v == viewBinding.buttonMainSignup) {
            registerUser();
        }
    }


    private void registerUser() {
        String ipemail = viewBinding.etEmail.getText().toString().trim();
        String ipname = viewBinding.etUsername.getText().toString().trim();
        String ipphone = viewBinding.etPhone.getText().toString().trim();
        String ippassword = viewBinding.etPassword.getText().toString().trim();
        String ipconfirmpassword = viewBinding.etConfirmPassword.getText().toString().trim();

        //check email
        if (ipemail.isEmpty()) {
            viewBinding.etEmail.setError("Email is required!");
            viewBinding.etEmail.requestFocus();
            return;
        }
        //check email pattern
        if (!Patterns.EMAIL_ADDRESS.matcher(ipemail).matches()) {
            viewBinding.etEmail.setError("Please provide valid email!");
            viewBinding.etEmail.requestFocus();
            return;
        }
        // check name
        if (ipname.isEmpty()) {
            viewBinding.etUsername.setError("Name is required!");
            viewBinding.etUsername.requestFocus();
            return;
        }
        //check iphone
        if (ipphone.isEmpty()) {
            viewBinding.etPhone.setError("Full name is required!");
            viewBinding.etPhone.requestFocus();
            return;
        }
        //check password
        if (ippassword.isEmpty()) {
            viewBinding.etPassword.setError("password is required!");
            viewBinding.etPassword.requestFocus();
            return;
        }
        //check confirmpassword
        if (ipconfirmpassword.isEmpty()) {
            viewBinding.etConfirmPassword.setError("Confirmpassword is required!");
            viewBinding.etConfirmPassword.requestFocus();
            return;
        }
        //check password is equals confirmpassword
        if (!ippassword.equals(ipconfirmpassword)) {
            viewBinding.etPassword.setError("password not match!");
            viewBinding.etPassword.requestFocus();
            System.out.println(ippassword);
            System.out.println(ipconfirmpassword);
            return;
        }
        if (ippassword.length() < 6) {
            viewBinding.etPassword.setError("Min password length should be 6 character!");
            viewBinding.etPassword.requestFocus();
            return;
        }
        viewBinding.progressBar.setVisibility(View.VISIBLE);

        mAuth.createUserWithEmailAndPassword(ipemail, ippassword)

                .addOnCompleteListener(new OnCompleteListener<AuthResult>() {
                    @Override
                    public void onComplete(@NonNull Task<AuthResult> task) {
                        String image = "N";

                        if (task.isSuccessful()) {
                            User user = new User(ipname, ipphone, image, "true");

                            FirebaseDatabase.getInstance().getReference("Users")
                                    .child(FirebaseAuth.getInstance().getCurrentUser().getUid())
                                    .setValue(user).addOnCompleteListener(new OnCompleteListener<Void>() {
                                @Override
                                public void onComplete(@NonNull Task<Void> task) {

                                    if (task.isSuccessful()) {
                                        Toast.makeText(getContext(), "Successfully and check email", Toast.LENGTH_LONG).show();
                                        viewBinding.progressBar.setVisibility(View.GONE);
                                        mAuth.signInWithEmailAndPassword(ipemail, ippassword).addOnCompleteListener(new OnCompleteListener<AuthResult>() {
                                            @Override
                                            public void onComplete(@NonNull Task<AuthResult> task) {
                                                if (task.isSuccessful()) {
                                                    FirebaseUser user = FirebaseAuth.getInstance().getCurrentUser();
                                                    //user is true, would login successfully
                                                    if (user.isEmailVerified()) {

                                                        //Set sharedPreferences
                                                        SharedPreferences pref = getActivity().getSharedPreferences("data", MODE_PRIVATE);
                                                        pref.edit().putString("Uid", user.getUid()).commit();

                                                        Intent intent = new Intent(getContext(), HomeActivity.class);
                                                        intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK | Intent.FLAG_ACTIVITY_NEW_TASK);
                                                        getContext().startActivity(intent);
                                                        Toast.makeText(getContext(), "Login successfully", Toast.LENGTH_LONG).show();

                                                    } else {
                                                        user.sendEmailVerification();
                                                        Toast.makeText(getContext(), "Check your email to verify you account!", Toast.LENGTH_LONG).show();
                                                        viewBinding.progressBar.setVisibility(View.GONE);
                                                    }

                                                } else {
                                                    //Check your email
                                                    //   Toast.makeText(getContext(),"Failed to login! Please check you credentials",Toast.LENGTH_LONG).show();
                                                    //  viewBinding.progressBar.setVisibility(View.GONE);
                                                }
                                            }
                                        });

                                    } else {
                                        Toast.makeText(getContext(), "Failed to register!!", Toast.LENGTH_LONG).show();
                                        viewBinding.progressBar.setVisibility(View.GONE);


                                    }
                                }
                            });


                        } else {
                            Toast.makeText(getContext(), "Failed to register!", Toast.LENGTH_LONG).show();
                            viewBinding.progressBar.setVisibility(View.GONE);
                        }

                    }
                });

    }
}
