package com.example.a300assignment_eshopnew.Sign;

import java.io.Serializable;

public class User implements Serializable {

    private String name,phone,image,normalUser;

    public User(String name, String phone, String image,String normalUser) {
        this.name = name;
        this.phone = phone;
        this.image = image;
        this.normalUser = normalUser;
    }

    public User() {

    }

    public String getName() {
        return name;
    }

    public User setName(String name) {
        this.name = name;
        return this;
    }

    public String getPhone() {
        return phone;
    }

    public User setPhone(String phone) {
        this.phone = phone;
        return this;
    }

    public String getImage() {
        return image;
    }

    public User setImage(String image) {
        this.image = image;
        return this;
    }

    public String getNormalUser() {
        return normalUser;
    }

    public User setNormalUser(String normalUser) {
        this.normalUser = normalUser;
        return this;
    }
}
