package com.example.a300assignment_eshopnew.base;

import android.os.Handler;
import android.os.Looper;
import android.view.LayoutInflater;
import android.view.ViewGroup;

import androidx.annotation.NonNull;
import androidx.annotation.UiThread;
import androidx.recyclerview.widget.RecyclerView;
import androidx.viewbinding.ViewBinding;

import java.lang.reflect.Method;
import java.lang.reflect.ParameterizedType;
import java.util.ArrayList;
import java.util.Collection;

public abstract class BaseAdapter<Data, VB extends ViewBinding> extends RecyclerView.Adapter<BaseViewHolder<VB>> {

    private ArrayList<Data> itemList = new ArrayList<>();
    private ArrayList<Data> originalItemList = new ArrayList<>();
    private final Handler mainThreadHandler = new Handler(Looper.getMainLooper());

    public BaseAdapter() {
        setHasStableIds(true);
    }

    @NonNull
    @Override
    public BaseViewHolder<VB> onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        VB viewBinding = null;
        try {
            ParameterizedType genericSuperclassType = (ParameterizedType) this.getClass().getGenericSuperclass();
            Class<VB> viewBindingClass = (Class<VB>) genericSuperclassType.getActualTypeArguments()[1];
            Method inflateMethod = viewBindingClass.getMethod("inflate", LayoutInflater.class, ViewGroup.class, boolean.class);
            viewBinding = (VB) inflateMethod.invoke(null, LayoutInflater.from(parent.getContext()), parent, false);
        } catch (Exception e) {
            e.printStackTrace();
        }
        return new BaseViewHolder<>(viewBinding.getRoot(), viewBinding);
    }

    @Override
    public long getItemId(int position) {
        return itemList.get(position).hashCode();
    }

    @Override
    public void onBindViewHolder(@NonNull BaseViewHolder<VB> holder, int position) {
        onBindData(holder.viewBinding, itemList.get(position), position);
    }

    protected abstract void onBindData(VB viewBinding, Data data, int position);

    @Override
    public int getItemCount() {
        return itemList.size();
    }

    public void clearAndAddData(ArrayList<Data> dataSet) {
        mainThreadHandler.post(new Runnable() {
            @Override
            public void run() {
                itemList = dataSet;
                notifyDataSetChanged();
            }
        });
    }

    public void addData(Collection<Data> dataSet) {
        mainThreadHandler.post(new Runnable() {
            @Override
            public void run() {
                for (Data data : dataSet) {
                    addDataInternal(data, itemList.size());
                }
            }
        });
    }

    public void addData(Data data) {
        mainThreadHandler.post(new Runnable() {
            @Override
            public void run() {
                addDataInternal(data, itemList.size());
            }
        });
    }

    public void addData(Data data, int position) {
        mainThreadHandler.post(new Runnable() {
            @Override
            public void run() {
                addDataInternal(data, position);
            }
        });
    }

    @UiThread
    public void addDataInternal(Data data) {
        addDataInternal(data, itemList.size());
    }

    @UiThread
    public void addDataInternal(Data data, int position) {
        itemList.add(position, data);
        originalItemList.add(position, data);
        notifyDataSetChanged();
    }

    @UiThread
    public void removeAllDataInternal() {
        itemList.clear();
        originalItemList.clear();
        notifyDataSetChanged();
    }

    public void removeAllData() {
        mainThreadHandler.post(new Runnable() {
            @Override
            public void run() {
                removeAllDataInternal();
            }
        });
    }

    public void removeData(int position) {
        mainThreadHandler.post(new Runnable() {
            @Override
            public void run() {
                itemList.remove(position);
                originalItemList.remove(position);
                notifyItemRemoved(position);
                notifyItemRangeChanged(position, itemList.size());
            }
        });
    }

    public Data getItem(int position) {
        return itemList.get(position);
    }

    public void performFiltering(OnFilterListener<Data> listener) {
        clearAndAddData(new ArrayList<>(listener.onFilter(originalItemList)));
    }

    public void clearFilter() {
        clearAndAddData(originalItemList);
    }




}
