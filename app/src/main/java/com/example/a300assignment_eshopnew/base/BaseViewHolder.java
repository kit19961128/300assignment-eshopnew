package com.example.a300assignment_eshopnew.base;

import android.view.View;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;
import androidx.viewbinding.ViewBinding;

public class BaseViewHolder<VB extends ViewBinding> extends RecyclerView.ViewHolder {

    public VB viewBinding;

    public BaseViewHolder(@NonNull View itemView, VB viewBinding) {
        super(itemView);
        this.viewBinding = viewBinding;
    }
}
