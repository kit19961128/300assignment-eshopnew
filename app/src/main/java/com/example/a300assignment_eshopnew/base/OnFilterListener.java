package com.example.a300assignment_eshopnew.base;

import java.util.List;

public interface OnFilterListener<Data> {

    public List<Data> onFilter(List<Data> dataList);
}
