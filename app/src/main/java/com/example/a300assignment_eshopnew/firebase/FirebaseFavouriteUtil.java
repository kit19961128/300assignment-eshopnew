package com.example.a300assignment_eshopnew.firebase;

import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;

public class FirebaseFavouriteUtil {
    public static DatabaseReference getFavouriteDatabase(String userId) {
        return FirebaseDatabase.getInstance().getReference().child("Favourite List").child(userId);
    }
}
