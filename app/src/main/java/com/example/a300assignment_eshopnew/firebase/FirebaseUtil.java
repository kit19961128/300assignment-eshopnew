package com.example.a300assignment_eshopnew.firebase;

import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;

public class FirebaseUtil {
    public static DatabaseReference getCartDatabase(String userId) {
        return FirebaseDatabase.getInstance().getReference().child("Cart List").child(userId);
    }
}
