package com.example.a300assignment_eshopnew.home;

public class News {

    int image;
    String title,price;

    public News(int image, String title, String price) {
        this.image = image;
        this.title = title;
        this.price = price;
    }

    public int getImage() {
        return image;
    }

    public String getTitle() {
        return title;
    }

    public String getPrice() {
        return price;
    }
}
