package com.example.a300assignment_eshopnew.home;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.cardview.widget.CardView;
import androidx.recyclerview.widget.RecyclerView;

import com.example.a300assignment_eshopnew.R;
import com.example.a300assignment_eshopnew.product.Product;
import com.example.a300assignment_eshopnew.product.Productdetail.ProductdetailActivity;
import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;
import com.squareup.picasso.Picasso;

import java.io.Serializable;
import java.util.ArrayList;

public class NewsAdapter extends RecyclerView.Adapter<NewsAdapter.FeaturedViewHolder> {

    ArrayList<Product> news;
    private Context context;
    private String userID;

    public NewsAdapter(Context context, ArrayList<Product> news) {
        this.news = news;
        this.context = context;
        userID = context.getSharedPreferences("data", Context.MODE_PRIVATE).getString("Uid", "");
    }

    @NonNull
    @Override
    public FeaturedViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        //set the layout id
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.homepage_news_card_design, parent, false);
        return new FeaturedViewHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull FeaturedViewHolder holder, int position) {

        Product product = news.get(position);
        //set the homepage-event-card image, title, background
        holder.parent.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(context, ProductdetailActivity.class);
                intent.putExtra("Product", product);
                context.startActivity(intent);
                ((Activity) context).overridePendingTransition(R.anim.slide_in_right, R.anim.slide_out_left);
            }
        });
        Picasso.with(context).load(product.getImage()).into(holder.image);
        holder.title.setText(product.getProductName());
        holder.price.setText("$" + product.getProductPrice());





        DatabaseReference rootRef = FirebaseDatabase.getInstance().getReference();
        DatabaseReference userNameRef = rootRef.child("Favourite List").child(userID).child(product.getPid());

        holder.heart.setImageResource(R.drawable.heart);
        ValueEventListener eventListener = new ValueEventListener() {
            @Override
            public void onDataChange(@NonNull DataSnapshot snapshot) {
                if (!snapshot.exists()) {
                    holder.heart.setImageResource(R.drawable.heart);
                } else {
                    holder.heart.setImageResource(R.drawable.ic_baseline_favorite_24);
                }
            }

            @Override
            public void onCancelled(@NonNull DatabaseError error) {

            }
        };


            userNameRef.addValueEventListener(eventListener);
            holder.heart.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    if (userID == "") {
                        Toast.makeText(context, "Please login first", Toast.LENGTH_LONG).show();
                    } else {
                        ValueEventListener eventListener1 = new ValueEventListener() {
                            public void onDataChange(DataSnapshot dataSnapshot) {
                                if (!dataSnapshot.exists()) {
                                    userNameRef.setValue(product).addOnCompleteListener(new OnCompleteListener<Void>() {
                                        @Override
                                        public void onComplete(@NonNull Task<Void> task) {
                                            if (task.isSuccessful()) {
                                                Toast.makeText(context.getApplicationContext(), "Added to Favourite", Toast.LENGTH_LONG).show();

                                            } else {
                                                Toast.makeText(context.getApplicationContext(), "Failed to adding", Toast.LENGTH_LONG).show();
                                            }
                                        }
                                    });
                                } else {
                                    userNameRef.removeValue().addOnCompleteListener(new OnCompleteListener<Void>() {
                                        @Override
                                        public void onComplete(@NonNull Task<Void> task) {
                                            if (task.isSuccessful()) {
                                                Toast.makeText(context.getApplicationContext(), "Removed from Favourite", Toast.LENGTH_LONG).show();
                                            } else {
                                                Toast.makeText(context.getApplicationContext(), "Failed to removing", Toast.LENGTH_LONG).show();
                                            }
                                        }
                                    });
                                }
                            }


                            @Override
                            public void onCancelled(DatabaseError databaseError) {
                                Log.d("TAG", databaseError.getMessage()); //Don't ignore errors!
                            }
                        };

                        userNameRef.addListenerForSingleValueEvent(eventListener1);

                    }
                }
            });

    }

    @Override
    public int getItemCount() {
        return news.size();
    }

    public static class FeaturedViewHolder extends RecyclerView.ViewHolder {

        ImageView heart;
        ImageView image;
        TextView title;
        TextView price;
        CardView parent;

        public FeaturedViewHolder(@NonNull View itemView) {
            super(itemView);

            //find the item id

            parent = itemView.findViewById(R.id.parent);
            heart = itemView.findViewById(R.id.homePage_heart);
            image = itemView.findViewById(R.id.homePage_productImage);
            title = itemView.findViewById(R.id.homePage_productName);
            price = itemView.findViewById(R.id.homePage_productPrice);

        }
    }


}
