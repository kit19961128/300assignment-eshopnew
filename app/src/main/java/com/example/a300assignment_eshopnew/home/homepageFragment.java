package com.example.a300assignment_eshopnew.home;

import android.content.Intent;
import android.graphics.drawable.Drawable;
import android.os.Bundle;
import android.view.ActionProvider;
import android.view.ContextMenu;
import android.view.LayoutInflater;
import android.view.MenuItem;
import android.view.SubMenu;
import android.view.View;
import android.view.ViewGroup;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentManager;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.example.a300assignment_eshopnew.HomeActivity;
import com.example.a300assignment_eshopnew.R;
import com.example.a300assignment_eshopnew.Sign.User;
import com.example.a300assignment_eshopnew.databinding.FragmentHomepageBinding;
import com.example.a300assignment_eshopnew.product.Product;
import com.example.a300assignment_eshopnew.product.productFragment;
import com.example.a300assignment_eshopnew.utils.ActivityUtil;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;

import java.lang.reflect.Array;
import java.util.ArrayList;

public class homepageFragment extends Fragment implements View.OnClickListener {

    private DatabaseReference DB_reference;
    private FirebaseUser firebaseUser;
    private RecyclerView.Adapter adapter, adapter2;
    private FragmentHomepageBinding viewBinding;
    private String userID = "";
    private int unicode;
    private String emoji;

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        final View view = inflater.inflate(R.layout.fragment_homepage, container, false);
        viewBinding = FragmentHomepageBinding.bind(view);
        viewBinding.lampViewAll.setOnClickListener(this);
        viewBinding.sofaViewAll.setOnClickListener(this);

        firebaseUser = FirebaseAuth.getInstance().getCurrentUser();

        boolean emailVerified = false;
        if (firebaseUser != null) {
            userID = firebaseUser.getUid();
            emailVerified = firebaseUser.isEmailVerified();
        }
        //  if(firebaseUser != null||emailVerified){
        //      viewBinding.HomePageText.setText("HI!"+userID);
        //  }

        //get emoji by unicode
        unicode = 0x1F44B;
        emoji = getEmojiByUnicode(unicode);


        if (userID != "" || emailVerified) {

            DB_reference = FirebaseDatabase.getInstance().getReference("Users");

            DB_reference.child(userID).addValueEventListener(new ValueEventListener() {
                @Override
                public void onDataChange(@NonNull DataSnapshot snapshot) {
                    User userProfile = snapshot.getValue(User.class);
                    if (userProfile != null) {
                        String fullname = userProfile.getName();
                        viewBinding.HomePageText.setText("HI! " + fullname + emoji);
                    }
                }

                @Override
                public void onCancelled(@NonNull DatabaseError error) {

                }
            });
        }
        // setup RecyclerView
        viewBinding.newsRecycler.setHasFixedSize(true);
        viewBinding.newsRecycler.setLayoutManager(new LinearLayoutManager(getContext(), LinearLayoutManager.HORIZONTAL, false));

        viewBinding.newsRecycler2.setHasFixedSize(true);
        viewBinding.newsRecycler2.setLayoutManager(new LinearLayoutManager(getContext(), LinearLayoutManager.HORIZONTAL, false));

        DatabaseReference productDb = FirebaseDatabase.getInstance().getReference().child("Product");
        productDb.addValueEventListener(new ValueEventListener() {
            @Override
            public void onDataChange(@NonNull DataSnapshot snapshot) {
                ArrayList<Product> productList = new ArrayList<>();
                for (DataSnapshot datas : snapshot.getChildren()) {
                    Product product = datas.getValue(Product.class);
                    product.setPid(datas.getKey());
                    productList.add(product);
                }

                ArrayList<Product> lampList = new ArrayList<>();
                for (Product object : productList) {
                    if (object.getProductType().toLowerCase().matches("lamp")) {
                        lampList.add(object);
                    }
                }
                ArrayList<Product> sofaList = new ArrayList<>();
                for (Product object : productList) {
                    if (object.getProductType().toLowerCase().matches("sofa")) {
                        sofaList.add(object);
                    }
                }
                viewBinding.newsRecycler.setAdapter(new NewsAdapter(getContext(), lampList));
                viewBinding.newsRecycler2.setAdapter(new NewsAdapter(getContext(), sofaList));
            }

            @Override
            public void onCancelled(@NonNull DatabaseError error) {

            }
        });

        return view;
    }

    public String getEmojiByUnicode(int unicode) {
        return new String(Character.toChars(unicode));
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
    }

    @Override
    public void onClick(View v) {
        if (v == viewBinding.lampViewAll || v == viewBinding.sofaViewAll) {
            try {
                HomeActivity homeActivity = ActivityUtil.activity;
                if (homeActivity != null) {
                    homeActivity.selectNavigationViewItem(new MenuItem() {
                        @Override
                        public int getItemId() {
                            return R.id.nav_product;
                        }

                        @Override
                        public int getGroupId() {
                            return 0;
                        }

                        @Override
                        public int getOrder() {
                            return 0;
                        }

                        @Override
                        public MenuItem setTitle(CharSequence title) {
                            return null;
                        }

                        @Override
                        public MenuItem setTitle(int title) {
                            return null;
                        }

                        @Override
                        public CharSequence getTitle() {
                            return null;
                        }

                        @Override
                        public MenuItem setTitleCondensed(CharSequence title) {
                            return null;
                        }

                        @Override
                        public CharSequence getTitleCondensed() {
                            return null;
                        }

                        @Override
                        public MenuItem setIcon(Drawable icon) {
                            return null;
                        }

                        @Override
                        public MenuItem setIcon(int iconRes) {
                            return null;
                        }

                        @Override
                        public Drawable getIcon() {
                            return null;
                        }

                        @Override
                        public MenuItem setIntent(Intent intent) {
                            return null;
                        }

                        @Override
                        public Intent getIntent() {
                            return null;
                        }

                        @Override
                        public MenuItem setShortcut(char numericChar, char alphaChar) {
                            return null;
                        }

                        @Override
                        public MenuItem setNumericShortcut(char numericChar) {
                            return null;
                        }

                        @Override
                        public char getNumericShortcut() {
                            return 0;
                        }

                        @Override
                        public MenuItem setAlphabeticShortcut(char alphaChar) {
                            return null;
                        }

                        @Override
                        public char getAlphabeticShortcut() {
                            return 0;
                        }

                        @Override
                        public MenuItem setCheckable(boolean checkable) {
                            return null;
                        }

                        @Override
                        public boolean isCheckable() {
                            return false;
                        }

                        @Override
                        public MenuItem setChecked(boolean checked) {
                            return null;
                        }

                        @Override
                        public boolean isChecked() {
                            return false;
                        }

                        @Override
                        public MenuItem setVisible(boolean visible) {
                            return null;
                        }

                        @Override
                        public boolean isVisible() {
                            return false;
                        }

                        @Override
                        public MenuItem setEnabled(boolean enabled) {
                            return null;
                        }

                        @Override
                        public boolean isEnabled() {
                            return false;
                        }

                        @Override
                        public boolean hasSubMenu() {
                            return false;
                        }

                        @Override
                        public SubMenu getSubMenu() {
                            return null;
                        }

                        @Override
                        public MenuItem setOnMenuItemClickListener(OnMenuItemClickListener menuItemClickListener) {
                            return null;
                        }

                        @Override
                        public ContextMenu.ContextMenuInfo getMenuInfo() {
                            return null;
                        }

                        @Override
                        public void setShowAsAction(int actionEnum) {

                        }

                        @Override
                        public MenuItem setShowAsActionFlags(int actionEnum) {
                            return null;
                        }

                        @Override
                        public MenuItem setActionView(View view) {
                            return null;
                        }

                        @Override
                        public MenuItem setActionView(int resId) {
                            return null;
                        }

                        @Override
                        public View getActionView() {
                            return null;
                        }

                        @Override
                        public MenuItem setActionProvider(ActionProvider actionProvider) {
                            return null;
                        }

                        @Override
                        public ActionProvider getActionProvider() {
                            return null;
                        }

                        @Override
                        public boolean expandActionView() {
                            return false;
                        }

                        @Override
                        public boolean collapseActionView() {
                            return false;
                        }

                        @Override
                        public boolean isActionViewExpanded() {
                            return false;
                        }

                        @Override
                        public MenuItem setOnActionExpandListener(OnActionExpandListener listener) {
                            return null;
                        }
                    });
                }
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
    }
}