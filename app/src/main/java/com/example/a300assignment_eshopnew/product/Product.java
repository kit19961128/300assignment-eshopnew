package com.example.a300assignment_eshopnew.product;

import android.util.Log;

import java.io.Serializable;
import java.lang.reflect.Field;
import java.util.HashMap;

public class Product implements Serializable {
    private String ar_modelLink;
    private String color;
    private String productDescription;
    private String image;
    private String material;
    private String productName;
    private String productPrice;
    private String productType;
    private String size;
    private String star;
    private String pid;

    public Product() {
    }

    public Product(String ar_modelLink, String color, String productDescription, String image, String material, String productName, String productPrice, String productType, String size, String star) {
        this.ar_modelLink = ar_modelLink;
        this.color = color;
        this.productDescription = productDescription;
        this.image = image;
        this.material = material;
        this.productName = productName;
        this.productPrice = productPrice;
        this.productType = productType;
        this.size = size;
        this.star = star;
    }


    public String getAr_modelLink() {
        return ar_modelLink;
    }

    public String getColor() {
        return color;
    }

    public String getProductDescription() {
        return productDescription;
    }

    public String getImage() {
        return image;
    }

    public String getMaterial() {
        return material;
    }

    public String getProductName() {
        return productName;
    }

    public String getProductPrice() {
        return productPrice;
    }

    public String getProductType() {
        return productType;
    }

    public String getSize() {
        return size;
    }

    public String getStar() {
        return star;
    }

    public String getPid() {
        return pid;
    }

    public void setPid(String pid) {
        this.pid = pid;
    }

    public HashMap<String, Object> toMap() {
        HashMap<String, Object> map = new HashMap<>();
        Field[] fields = this.getClass().getDeclaredFields();
        try {
            for (Field field : fields) {
                field.setAccessible(true);
                map.put(field.getName(),field.get(this));
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        Log.d("DEBUG","Product = " + map.toString());
        return map;
    }
}
