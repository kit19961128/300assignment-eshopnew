package com.example.a300assignment_eshopnew.product;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.util.Log;
import android.view.View;
import android.widget.Toast;

import androidx.annotation.NonNull;

import com.example.a300assignment_eshopnew.R;
import com.example.a300assignment_eshopnew.base.BaseAdapter;
import com.example.a300assignment_eshopnew.databinding.ProductCardDesignBinding;
import com.example.a300assignment_eshopnew.product.Productdetail.ProductdetailActivity;
import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;
import com.squareup.picasso.Picasso;

import java.io.Serializable;
import java.util.ArrayList;


public class ProductAdapter extends BaseAdapter<Product, ProductCardDesignBinding> {
    private Context context;
    private ArrayList<Product> products = new ArrayList<>();
    private ArrayList<Product> products2 = new ArrayList<>();


    public ProductAdapter(ArrayList<Product> products, Context context) {
        this.products = products;
        this.products2 = (ArrayList<Product>) products.clone();
        this.context = context;
        addData(products);
    }


    @Override
    protected void onBindData(ProductCardDesignBinding viewBinding, Product product, int position) {

        viewBinding.productName.setText(product.getProductName());
        viewBinding.productPrice.setText("$" + product.getProductPrice());
        viewBinding.productRating.setRating(Float.parseFloat(product.getStar()));
        //  viewBinding.productrating.setRating(Float.parseFloat(product.getStar()));

        //add product image
        Picasso.with(context).load(product.getImage()).into(viewBinding.productImage);


        //adapter need if and "else" checking , not only if checking
        //check product havn't ar
        if (product.getAr_modelLink().equals("N")) {
            viewBinding.imageAR.setVisibility(View.INVISIBLE);
        }else{
            viewBinding.imageAR.setVisibility(View.VISIBLE);
        }

        String userID = context.getSharedPreferences("data", Context.MODE_PRIVATE).getString("Uid", "");
        DatabaseReference rootRef = FirebaseDatabase.getInstance().getReference();
        DatabaseReference userNameRef = rootRef.child("Favourite List").child(userID).child(product.getPid());
        viewBinding.favouriteBorder.setImageResource(R.drawable.heart);
        ValueEventListener eventListener = new ValueEventListener() {

            @Override
            public void onDataChange(DataSnapshot dataSnapshot) {
                if (!dataSnapshot.exists()) {
                    viewBinding.favouriteBorder.setImageResource(R.drawable.heart);
                } else {
                    viewBinding.favouriteBorder.setImageResource(R.drawable.ic_baseline_favorite_24);
                }
            }

            @Override
            public void onCancelled(DatabaseError databaseError) {
                Log.d("TAG", databaseError.getMessage()); //Don't ignore errors!
            }
        };
        userNameRef.addValueEventListener(eventListener);

        viewBinding.layout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(context, ProductdetailActivity.class);
                intent.putExtra("Product", (Serializable) product);
                context.startActivity(intent);
                ((Activity) context).overridePendingTransition(R.anim.slide_in_right, R.anim.slide_out_left);

            }
        });

        viewBinding.favouriteBorder.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (userID == "") {
                    Toast.makeText(context, "Please login first", Toast.LENGTH_LONG).show();
                } else {
                    ValueEventListener eventListener1 = new ValueEventListener() {
                        public void onDataChange(DataSnapshot dataSnapshot) {
                            if (!dataSnapshot.exists()) {
                                userNameRef.setValue(product).addOnCompleteListener(new OnCompleteListener<Void>() {
                                    @Override
                                    public void onComplete(@NonNull Task<Void> task) {
                                        if (task.isSuccessful()) {
                                            Toast.makeText(context.getApplicationContext(), "Added to Favourite", Toast.LENGTH_LONG).show();

                                        } else {
                                            Toast.makeText(context.getApplicationContext(), "Failed to adding", Toast.LENGTH_LONG).show();
                                        }
                                    }
                                });
                            } else {
                                userNameRef.removeValue().addOnCompleteListener(new OnCompleteListener<Void>() {
                                    @Override
                                    public void onComplete(@NonNull Task<Void> task) {
                                        if (task.isSuccessful()) {
                                            Toast.makeText(context.getApplicationContext(), "Removed from Favourite", Toast.LENGTH_LONG).show();
                                        } else {
                                            Toast.makeText(context.getApplicationContext(), "Failed to removing", Toast.LENGTH_LONG).show();
                                        }
                                    }
                                });
                            }
                        }


                        @Override
                        public void onCancelled(DatabaseError databaseError) {
                            Log.d("TAG", databaseError.getMessage()); //Don't ignore errors!
                        }
                    };
                    userNameRef.addListenerForSingleValueEvent(eventListener1);

                }
            }
        });
    }
}
//    @Override
//    public Filter getFilter() {
//        return new Filter() {
//            @Override
//            protected FilterResults performFiltering(CharSequence charSequence) {
//                String key = charSequence.toString();
//                ArrayList<Product> products;
//                if (key.isEmpty()) {
//                    products = products2;
//                } else {
//                    ArrayList<Product> products3 = new ArrayList<>();
//                    for (Product row : products2) {
//                        if (row.getProductName().toLowerCase().contains(key.toLowerCase())) {
//                            products3.add(row);
//                        }
//                    }
//                    products = products3;
//                }
//
//                FilterResults filterResults = new FilterResults();
//                filterResults.values = products;
//                return filterResults;
//            }
//
//            @Override
//            protected void publishResults(CharSequence charSequence, FilterResults filterResults) {
//                clearAndAddData((ArrayList<Product>) filterResults.values);
////                notifyDataSetChanged();
//            }
//        };
//    }

