package com.example.a300assignment_eshopnew.product.Productdetail;

import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.Spinner;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;

import com.cepheuen.elegantnumberbutton.view.ElegantNumberButton;
import com.example.a300assignment_eshopnew.ARcore.arcoreActivity;
import com.example.a300assignment_eshopnew.R;
import com.example.a300assignment_eshopnew.databinding.ActivityProductdetailBinding;
import com.example.a300assignment_eshopnew.firebase.FirebaseUtil;
import com.example.a300assignment_eshopnew.product.Product;
import com.example.a300assignment_eshopnew.product.ShoppingCart.Cart;
import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.auth.FirebaseUser;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;
import com.squareup.picasso.Picasso;

import java.io.Serializable;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.UUID;

public class ProductdetailActivity extends AppCompatActivity {
    private Product product;


    //Save size item
    ArrayList<String> addsize = new ArrayList<String>();

    private Spinner productSize;
    private ImageView productImage, productAR, favourite_border;
    private ElegantNumberButton productQuantity;
    private String userID;


    private String favourite;

    private DatabaseReference databaseReference;
    // private FirebaseDatabase database = FirebaseDatabase.getInstance();

    ActivityProductdetailBinding viewBinding;

    // Firebase login user
    private FirebaseUser user;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        viewBinding = ActivityProductdetailBinding.inflate((LayoutInflater.from(this)));
        setContentView(viewBinding.getRoot());

        userID = getSharedPreferences("data", MODE_PRIVATE).getString("Uid", "");

        //   favourite = findViewById(R.id.favourite);
        favourite_border = findViewById(R.id.favourite_border);
        productQuantity = findViewById(R.id.productQuantity);
        productSize = findViewById(R.id.productSize);
        productImage = findViewById(R.id.productImage);
        productAR = findViewById(R.id.productAR);

        Intent i = getIntent();
        product = (Product) i.getSerializableExtra("Product");


        viewBinding.productRating.setRating(Float.parseFloat(product.getStar()));
        viewBinding.TopProductName.setText(product.getProductName());
        viewBinding.productType.setText(product.getProductType());
        viewBinding.productName.setText(product.getProductName());
        viewBinding.productPrice.setText("$" + product.getProductPrice());
        viewBinding.productColor.setText(product.getColor());
        viewBinding.productMaterial.setText(product.getMaterial());
        viewBinding.productDescription.setText(product.getProductDescription());
        viewBinding.productStar.setText(product.getStar());

        //add size in spinner
        addsize = getSize(product.getSize());
        //add image
        Picasso.with(this).load(product.getImage()).into(productImage);


        //Add the size to the spinner
        ArrayAdapter<String> adapter = new ArrayAdapter<String>(getApplicationContext(), android.R.layout.simple_spinner_dropdown_item, addsize);
        adapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        productSize.setAdapter(adapter);
        monitorFavouriteStatus();

        //add to cart and check login
        addToCart();
        addToFavourite();


        //check product isn't 3D model
        if (!product.getAr_modelLink().equals("N")) {
            productAR.setVisibility(View.VISIBLE);
            productAR.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    Intent intent = new Intent(getApplicationContext(), arcoreActivity.class);
                    intent.putExtra("productAR", (Serializable) product);
                    startActivity(intent);
                    overridePendingTransition(R.anim.slide_in_right, R.anim.slide_out_left);
                }
            });
        } else {
            productAR.setVisibility(View.INVISIBLE);
        }


    }


    //favourite checking

    private void addToFavourite() {
        viewBinding.favouriteBorder.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View v) {
                if (userID == "") {
                    Toast.makeText(getApplicationContext(), "Please login first", Toast.LENGTH_LONG).show();
                } else {

                    checkingFavourite();

                }
            }
        });
    }


    // adding favourite to firebase
    private void checkingFavourite() {

        DatabaseReference rootRef = FirebaseDatabase.getInstance().getReference();
        DatabaseReference userNameRef = rootRef.child("Favourite List").child(userID).child(product.getPid());
        ValueEventListener eventListener = new ValueEventListener() {

            @Override
            public void onDataChange(DataSnapshot dataSnapshot) {
                if (!dataSnapshot.exists()) {
                    userNameRef.setValue(product).addOnCompleteListener(new OnCompleteListener<Void>() {
                        @Override
                        public void onComplete(@NonNull Task<Void> task) {
                            if (task.isSuccessful()) {
                                Toast.makeText(getApplicationContext(), "Added to Favourite", Toast.LENGTH_LONG).show();

                            } else {
                                Toast.makeText(getApplicationContext(), "Failed to adding", Toast.LENGTH_LONG).show();
                            }
                        }
                    });
                } else {
                    userNameRef.removeValue().addOnCompleteListener(new OnCompleteListener<Void>() {
                        @Override
                        public void onComplete(@NonNull Task<Void> task) {
                            if (task.isSuccessful()) {
                                Toast.makeText(getApplicationContext(), "Removed from Favourite", Toast.LENGTH_LONG).show();

                            } else {
                                Toast.makeText(getApplicationContext(), "Failed to removing", Toast.LENGTH_LONG).show();
                            }
                        }
                    });
                }
            }

            @Override
            public void onCancelled(DatabaseError databaseError) {
                Log.d("TAG", databaseError.getMessage()); //Don't ignore errors!
            }
        };
        userNameRef.addListenerForSingleValueEvent(eventListener);

    }

    private void monitorFavouriteStatus() {
        DatabaseReference rootRef = FirebaseDatabase.getInstance().getReference();
        DatabaseReference userNameRef = rootRef.child("Favourite List").child(userID).child(product.getPid());
        ValueEventListener eventListener = new ValueEventListener() {

            @Override
            public void onDataChange(DataSnapshot dataSnapshot) {
                if (!dataSnapshot.exists()) {
                    //create new use
                    favourite_border.setImageResource(R.drawable.heart);
                } else {
                    favourite_border.setImageResource(R.drawable.ic_baseline_favorite_24);

                }
            }

            @Override
            public void onCancelled(DatabaseError databaseError) {
                Log.d("TAG", databaseError.getMessage()); //Don't ignore errors!
            }
        };
        userNameRef.addValueEventListener(eventListener);
    }


    //button add
    private void addToCart() {
        viewBinding.btnAddToCart.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (userID == "") {
                    Toast.makeText(getApplicationContext(), "Please login first", Toast.LENGTH_LONG).show();
                } else {
                    addingToCartList();
                }
            }
        });
    }

    //Function add to cart
    private void addingToCartList() {
        String CurrentTime, CurrentDate;
        Calendar calForDate = Calendar.getInstance();
        //instance cart list
        databaseReference = FirebaseUtil.getCartDatabase(userID);

        SimpleDateFormat currentDate = new SimpleDateFormat("MMM dd, yyyy");
        CurrentDate = currentDate.format(calForDate.getTime());

        SimpleDateFormat currentTime = new SimpleDateFormat("HH:mm:ss a");
        CurrentTime = currentTime.format(calForDate.getTime());

        String selectedSize = (String) viewBinding.productSize.getSelectedItem();
        //  final HashMap<String, Object> cartMap = new HashMap<>();
        String uuid = UUID.randomUUID().toString();
        Cart addToCart = new Cart();
        addToCart.setId(uuid)
                .setProduct(product)
                .setSize(selectedSize)
                .setDate(CurrentDate)
                .setTime(CurrentTime)
                .setQuantity(productQuantity.getNumber());

        //  cartMap.put("date", CurrentDate);
        //   cartMap.put("time", CurrentTime);
        //   cartMap.put("productname", product.getProductName());
        //   cartMap.put("productprice", product.getProductPrice());
        //   cartMap.put("producttype", product.getProductType());
        //   cartMap.put("material",product.getMaterial());
        //   cartMap.put("color", product.getColor());
        //    cartMap.put("image", product.getImage());
        //   cartMap.put("size", product.getSize());
        // cartMap.put("quantity", productQuantity.getNumber());


        //update cart list
        databaseReference.child(uuid).setValue(addToCart).addOnCompleteListener(new OnCompleteListener<Void>() {
            @Override
            public void onComplete(@NonNull Task<Void> task) {
                if (task.isSuccessful()) {
                    Toast.makeText(getApplicationContext(), "Added to cart", Toast.LENGTH_LONG).show();
                    finish();
                }
            }
        });
        //databaseReference.child(userID).child(product.getPid())
        //        .updateChildren(product.toMap())
        //        .addOnCompleteListener(new OnCompleteListener<Void>() {
        //            @Override
        //            public void onComplete(@NonNull Task<Void> task) {
        //                if (task.isSuccessful()) {
        //                    Toast.makeText(getApplicationContext(), "Added to cart", Toast.LENGTH_LONG).show();
        //                    finish();
        //                }
        //            }
        //        });
    }

    //split the product size
    private ArrayList<String> getSize(String size) {
        ArrayList<String> sizelist = new ArrayList<String>();

        for (String retval : size.split(",")) {
            sizelist.add(retval);
        }
        return sizelist;
    }

    //OnClick button back page
    public void backbutton(View view) {
        finish();
    }

    @Override
    public void finish() {
        super.finish();
        overridePendingTransition(R.anim.slide_in_left, R.anim.slide_out_right);
    }
}