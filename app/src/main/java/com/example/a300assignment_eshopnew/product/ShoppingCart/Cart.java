package com.example.a300assignment_eshopnew.product.ShoppingCart;

import android.util.Log;

import com.example.a300assignment_eshopnew.product.Product;

import java.io.Serializable;
import java.lang.reflect.Field;
import java.util.HashMap;

public class Cart implements Serializable {
    private String id;
    private Product product;
    private String date;
    private String time;
    private String size;
    private String quantity;


    public Cart() {
    }

    public String getId() {
        return id;
    }

    public Cart setId(String id) {
        this.id = id;
        return this;
    }

    public String getDate() {
        return date;
    }

    public Cart setDate(String date) {
        this.date = date;
        return this;
    }

    public String getTime() {
        return time;
    }

    public Cart setTime(String time) {
        this.time = time;
        return this;
    }

    public Product getProduct() {
        return product;
    }

    public Cart setProduct(Product product) {
        this.product = product;
        return this;
    }

    public String getQuantity() {
        return quantity;
    }

    public Cart setQuantity(String quantity) {
        this.quantity = quantity;
        return this;
    }

    public String getSize() {
        return size;
    }

    public Cart setSize(String size) {
        this.size = size;
        return this;
    }

    public HashMap<String, Object> toMap() {
        HashMap<String, Object> map = new HashMap<>();
        Field[] fields = this.getClass().getDeclaredFields();
        try {
            for (Field field : fields) {
                field.setAccessible(true);
                map.put(field.getName(), field.get(this));
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        Log.d("DEBUG", "Product = " + map.toString());
        return map;
    }
}