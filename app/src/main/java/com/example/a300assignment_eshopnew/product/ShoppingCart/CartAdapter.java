package com.example.a300assignment_eshopnew.product.ShoppingCart;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RatingBar;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.example.a300assignment_eshopnew.R;
import com.example.a300assignment_eshopnew.product.Product;
import com.example.a300assignment_eshopnew.product.Productdetail.ProductdetailActivity;
import com.squareup.picasso.Picasso;

import java.io.Serializable;
import java.util.ArrayList;


public class CartAdapter extends RecyclerView.Adapter<CartAdapter.newsviewHolder>  {
    private Context context;
    private ArrayList<Cart> carts = new ArrayList<>();
    private ArrayList<Cart> carts2 = new ArrayList<>();


    public CartAdapter(ArrayList<Cart> carts, Context context){
        this.carts = carts;
        this.carts2 = carts;
        this.context = context;
    }

    @NonNull
    @Override
    public newsviewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {

        //Add the layout to the listview
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.cart_items_layout,parent,false);
        newsviewHolder newsviewHolder = new newsviewHolder(view);
        return newsviewHolder;
    }

    @Override
    public void onBindViewHolder(@NonNull newsviewHolder holder, int position) {
        Cart cart = carts.get(position);

        Product product = cart.getProduct();
        //Add the Text
        holder.productName.setText(product.getProductName());
        holder.productPrice.setText("$"+product.getProductPrice());
       // holder.productDescription.setText(product.getDescription());

        //Add product image
        Picasso.with(context).load(product.getImage()).into(holder.productImage);



        holder.layout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(context, ProductdetailActivity.class);
                intent.putExtra("Product", (Serializable) cart);
                context.startActivity(intent);
                ((Activity)context).overridePendingTransition(R.anim.slide_in_right,R.anim.slide_out_left);

            }
        });


    }

    @Override
    public int getItemCount() {
        return carts.size();
    }



    public class newsviewHolder extends RecyclerView.ViewHolder {
        //Image
        ImageView productImage, imageAR;
        //Text
        TextView productName, productPrice, productDescription;
        //Rating Bar
        RatingBar rating;
        // RatingBar productrating;
        //Layout
        LinearLayout layout;

        public newsviewHolder(@NonNull View itemView) {
            super(itemView);

            //set the id
            productImage = itemView.findViewById(R.id.productImage);
            productName = itemView.findViewById(R.id.productName);
            productPrice = itemView.findViewById(R.id.productPrice);
           // productDescription = itemView.findViewById(R.id.productDescription);
            rating = itemView.findViewById(R.id.productRating);
            //   productrating = itemView.findViewById(R.id.productrating);
            imageAR = itemView.findViewById(R.id.imageAR);
            layout = itemView.findViewById(R.id.layout);

        }
    }

}
