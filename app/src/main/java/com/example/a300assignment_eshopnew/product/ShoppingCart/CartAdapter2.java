package com.example.a300assignment_eshopnew.product.ShoppingCart;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.view.View;

import com.example.a300assignment_eshopnew.R;
import com.example.a300assignment_eshopnew.base.BaseAdapter;
import com.example.a300assignment_eshopnew.databinding.CartItemsLayoutBinding;
import com.example.a300assignment_eshopnew.product.Product;
import com.squareup.picasso.Picasso;

public class CartAdapter2 extends BaseAdapter<Cart, CartItemsLayoutBinding> {

    protected Context context;

    private CartAdapter2() {
    }

    public CartAdapter2(Context context) {
        this.context = context;
    }

    String proudctCartID;

    @Override
    protected void onBindData(CartItemsLayoutBinding viewBinding, Cart cart, int position) {
        Product product = cart.getProduct();


        //Add the Text
        viewBinding.productName.setText(cart.getProduct().getProductName());
        viewBinding.productPrice.setText("$" + cart.getProduct().getProductPrice());
        viewBinding.productColor.setText("Color:  " + cart.getProduct().getColor());
        viewBinding.productMaterial.setText("Material:  " + cart.getProduct().getMaterial());
        //Quantity and Size should be in userCart
        viewBinding.productQuantity.setText("Quantity:  " + cart.getQuantity());
        viewBinding.productSize.setText("Size:  " + cart.getSize());
        // holder.productDescription.setText(product.getDescription());

        //Add product image
        Picasso.with(context).load(cart.getProduct().getImage()).into(viewBinding.productImage);


        viewBinding.layout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(context, CartUpdateActivity.class);
                intent.putExtra("Cart", cart);
                context.startActivity(intent);
                ((Activity) context).overridePendingTransition(R.anim.slide_in_right, R.anim.slide_out_left);

            }
        });

    }
}
