package com.example.a300assignment_eshopnew.product.ShoppingCart;

import android.content.Intent;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.LinearLayoutManager;

import com.example.a300assignment_eshopnew.R;
import com.example.a300assignment_eshopnew.SharePreferenceUtil;
import com.example.a300assignment_eshopnew.databinding.ActivityCartFragmentBinding;
import com.example.a300assignment_eshopnew.firebase.FirebaseUtil;
import com.example.a300assignment_eshopnew.swipe.RecyclerTouchListener;
import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.ValueEventListener;

public class CartFragment extends Fragment implements AdapterView.OnItemSelectedListener {

    private String userID = "";

    private DatabaseReference DB_eference;
    private CartAdapter2 adapter;

    private ArrayAdapter<CharSequence> spad;

    private SharePreferenceUtil sharePreferenceUtil;

    private ValueEventListener listener;
    ActivityCartFragmentBinding viewbinding;

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        final View view = inflater.inflate(R.layout.activity_cart_fragment, container, false);
        viewbinding = ActivityCartFragmentBinding.bind(view);

        sharePreferenceUtil = new SharePreferenceUtil(this.requireContext());
        // SharedPreferences cart = getActivity().getPreferences("cart", HomeActivity.MODE_PRIVATE);
        // userID = getSharedPreferences("data",MODE_PRIVATE).getString("Uid","");

        userID = sharePreferenceUtil.get("user", "userID", "");

        CartListRecycler();

        return view;
    }


    private void CartListRecycler() {
        viewbinding.cartListView.setHasFixedSize(true);
        viewbinding.cartListView.setLayoutManager(new LinearLayoutManager(getContext(), LinearLayoutManager.VERTICAL, false));
        adapter = new CartAdapter2(getContext());
        viewbinding.cartListView.setAdapter(adapter);
        RecyclerTouchListener touchListener = new RecyclerTouchListener(getActivity(), viewbinding.cartListView);
        touchListener
                .setClickable(new RecyclerTouchListener.OnRowClickListener() {
                    @Override
                    public void onRowClicked(int position) {
                    }

                    @Override
                    public void onIndependentViewClicked(int independentViewID, int position) {

                    }
                })
                .setSwipeOptionViews(R.id.delete_task, R.id.edit_task)
                .setSwipeable(R.id.layout, R.id.rowBG, new RecyclerTouchListener.OnSwipeOptionsClickListener() {
                    @Override
                    public void onSwipeOptionClicked(int viewID, int position) {
                        Cart cart = adapter.getItem(position);
                        if (viewID == R.id.delete_task) {
                            FirebaseUtil.getCartDatabase(userID).child(cart.getId()).removeValue().addOnCompleteListener(new OnCompleteListener<Void>() {
                                @Override
                                public void onComplete(@NonNull Task<Void> task) {
                                    if (task.isSuccessful()) {
                                        adapter.removeData(position);
                                        Toast.makeText(getContext(), "Removed from cart", Toast.LENGTH_LONG).show();
                                    }
                                }
                            });
                        } else if (viewID == R.id.edit_task) {
                            Intent intent = new Intent(getContext(), CartUpdateActivity.class);
                            intent.putExtra("Cart",cart);
                            startActivity(intent);
                        }
                    }
                });
        viewbinding.cartListView.addOnItemTouchListener(touchListener);
    }

    @Override
    public void onResume() {
        super.onResume();
        //Call firebase
        DB_eference = FirebaseUtil.getCartDatabase(userID);
        DB_eference.get().addOnCompleteListener(new OnCompleteListener<DataSnapshot>() {
            @Override
            public void onComplete(@NonNull Task<DataSnapshot> task) {
                if (task.isSuccessful()) {
                    DataSnapshot snapshot = task.getResult();
                    adapter.removeAllData();
                    for (DataSnapshot snapshot1 : snapshot.getChildren()) {
                        Cart cart = snapshot1.getValue(Cart.class);
                        adapter.addData(cart);
                    }

                }
            }
        });
        // listener = new ValueEventListener() {
        //     @Override
        //     public void onDataChange(@NonNull DataSnapshot snapshot) {
        //         adapter.removeAllData();
        //         for (DataSnapshot snapshot1 : snapshot.getChildren()) {
        //             Cart cart = snapshot1.getValue(Cart.class);
        //             adapter.addData(cart);
        //         }
        //         // for (DataSnapshot datas : snapshot.getChildren()) {
        //         //     Cart cart = datas.getValue(Cart.class);
        //         //     cart.setPid(datas.getKey());
        //         //     cartList.add(cart);
        //     }
        //
        //     @Override
        //     public void onCancelled(@NonNull DatabaseError error) {
//
        //     }
        // };
        //Get the firebase data
        //DB_eference.addValueEventListener(listener);

    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        if (DB_eference != null && listener != null) {
            DB_eference.removeEventListener(listener);
        }
    }


    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
    }


    @Override
    public void onItemSelected(AdapterView<?> adapterView, View view, int i, long l) {


    }

    @Override
    public void onNothingSelected(AdapterView<?> adapterView) {

    }


}