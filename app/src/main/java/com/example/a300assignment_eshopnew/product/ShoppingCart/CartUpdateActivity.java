package com.example.a300assignment_eshopnew.product.ShoppingCart;

import android.content.Intent;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.Spinner;
import android.widget.Toast;

import androidx.appcompat.app.AppCompatActivity;

import com.cepheuen.elegantnumberbutton.view.ElegantNumberButton;
import com.example.a300assignment_eshopnew.R;
import com.example.a300assignment_eshopnew.product.Product;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.squareup.picasso.Picasso;

import java.io.Serializable;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;

public class CartUpdateActivity extends AppCompatActivity {

    private Cart cart;


    //Save size item
    ArrayList<String> addsize = new ArrayList<String>();

    private Spinner productSize;
    private ImageView productImage, productAR;
    private ElegantNumberButton productQuantity;


    private DatabaseReference databaseReference;

    com.example.a300assignment_eshopnew.databinding.ActivityCartUpdateBinding viewBinding;

    // Firebase login user


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        viewBinding = com.example.a300assignment_eshopnew.databinding.ActivityCartUpdateBinding.inflate((LayoutInflater.from(this)));
        setContentView(viewBinding.getRoot());
        cart = (Cart) getIntent().getSerializableExtra("Cart");
        Product product = cart.getProduct();
        // userID = getSharedPreferences("data", MODE_PRIVATE).getString("Uid", "");

        productQuantity = findViewById(R.id.productQuantity);
        productSize = findViewById(R.id.productSize);
        productImage = findViewById(R.id.productImage);
        productAR = findViewById(R.id.productAR);


        productQuantity.setNumber(cart.getQuantity());
        // Intent i = getIntent();
        // product = (Product) i.getSerializableExtra("Product");

        viewBinding.productRating.setRating(Float.parseFloat(product.getStar()));
        viewBinding.TopProductName.setText(product.getProductName());
        viewBinding.productType.setText(product.getProductType());
        viewBinding.productName.setText(product.getProductName());
        viewBinding.productPrice.setText("$" + product.getProductPrice());
        viewBinding.productColor.setText(product.getColor());
        viewBinding.productMaterial.setText(product.getMaterial());
        viewBinding.productDescription.setText(product.getProductDescription());
        viewBinding.productStar.setText(product.getStar());


        //add size in spinner
        addsize = getSize(product.getSize());
        //add image
        Picasso.with(this).load(product.getImage()).into(productImage);


        //Add the size to the spinner
        ArrayAdapter<String> adapter = new ArrayAdapter<String>(getApplicationContext(), android.R.layout.simple_spinner_dropdown_item, addsize);
        adapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        productSize.setAdapter(adapter);
        int selectedSizeIndex = addsize.indexOf(cart.getSize());
        if (selectedSizeIndex != -1) {
            productSize.setSelection(selectedSizeIndex);
        }
        //add to cart and check login
        addToCart();

        //check product isn't 3D model
        if (!product.getAr_modelLink().equals("N")) {
            productAR.setVisibility(View.VISIBLE);
            productAR.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    Intent intent = new Intent(getApplicationContext(), com.example.a300assignment_eshopnew.ARcore.arcoreActivity.class);
                    intent.putExtra("productAR", (Serializable) product);
                    startActivity(intent);
                    overridePendingTransition(R.anim.slide_in_right, R.anim.slide_out_left);
                }
            });
        } else {
            productAR.setVisibility(View.INVISIBLE);
        }


    }

    //button add
    private void addToCart() {
        viewBinding.btnUpdateCart.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                UpdateCart();

            }
        });
    }


    private void UpdateCart() {
        String CurrentTime, CurrentDate;
        Calendar calForDate = Calendar.getInstance();

        FirebaseAuth mAuth = FirebaseAuth.getInstance();
        FirebaseUser firebaseUser = mAuth.getCurrentUser();

        String userID = firebaseUser.getUid();


        String selectedSize = (String) viewBinding.productSize.getSelectedItem();
        String selectedQuantity = (String) viewBinding.productQuantity.getNumber();

        SimpleDateFormat currentDate = new SimpleDateFormat("MMM dd, yyyy");
        CurrentDate = currentDate.format(calForDate.getTime());

        SimpleDateFormat currentTime = new SimpleDateFormat("HH:mm:ss a");
        CurrentTime = currentTime.format(calForDate.getTime());

        //get default firebasedatabase instance
        FirebaseDatabase mFirebaseDatabase = FirebaseDatabase.getInstance();

        //getdata and pass to edit text
        DatabaseReference databaseReference = mFirebaseDatabase.getReference().child("Cart List");

        databaseReference.child(userID).child(cart.getId()).child("date").setValue(CurrentDate);
        databaseReference.child(userID).child(cart.getId()).child("time").setValue(CurrentTime);
        databaseReference.child(userID).child(cart.getId()).child("size").setValue(selectedSize);
        databaseReference.child(userID).child(cart.getId()).child("quantity").setValue(selectedQuantity);

        Toast.makeText(this, "Cart has been update", Toast.LENGTH_LONG).show();
        finish();


    }


    //Function add to cart

    //databaseReference.child(userID).child(product.getPid())
    //        .updateChildren(product.toMap())
    //        .addOnCompleteListener(new OnCompleteListener<Void>() {
    //            @Override
    //            public void onComplete(@NonNull Task<Void> task) {
    //                if (task.isSuccessful()) {
    //                    Toast.makeText(getApplicationContext(), "Added to cart", Toast.LENGTH_LONG).show();
    //                    finish();
    //                }
    //            }
    //        });


    //split the product size
    private ArrayList<String> getSize(String size) {
        ArrayList<String> sizelist = new ArrayList<String>();

        for (String retval : size.split(",")) {
            sizelist.add(retval);
        }
        return sizelist;
    }

    //OnClick button back page
    public void backbutton(View view) {
        finish();
    }

    @Override
    public void finish() {
        super.finish();
        overridePendingTransition(R.anim.slide_in_left, R.anim.slide_out_right);
    }
}