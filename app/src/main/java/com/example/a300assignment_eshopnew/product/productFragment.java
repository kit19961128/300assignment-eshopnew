package com.example.a300assignment_eshopnew.product;

import android.content.Intent;
import android.os.Bundle;
import android.speech.RecognizerIntent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.ImageButton;
import android.widget.SearchView;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.example.a300assignment_eshopnew.R;
import com.example.a300assignment_eshopnew.base.OnFilterListener;
import com.example.a300assignment_eshopnew.databinding.FragmentProductBinding;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;

import java.util.ArrayList;
import java.util.List;
import java.util.Locale;

public class productFragment extends Fragment implements AdapterView.OnItemSelectedListener {
    //mic
    private static final int REQUEST_CODE_SPEECH_INPUT = 1000;

    private RecyclerView productRecycler;
    private ProductAdapter adapter;
    private ImageButton btn_voice;

    private final ArrayList<Product> listproduct = new ArrayList<>();
    private final ArrayList<Product> listproduct2 = new ArrayList<>();

    private DatabaseReference reference;

    private ArrayAdapter<CharSequence> spad;
    private SearchView searchView;
    FragmentProductBinding viewbinding;

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        final View view = inflater.inflate(R.layout.fragment_product, container, false);
        viewbinding = FragmentProductBinding.bind(view);

        productRecycler = view.findViewById(R.id.product_Listview);
        //  searchView = (EditText)view.findViewById(R.id.search_bar);
        searchView = view.findViewById(R.id.search_bar);
        //   searchView.setLayoutParams(new Toolbar.LayoutParams(Gravity.RIGHT));
        //set spinner item
        searchView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                searchView.setIconified(false);
            }
        });
        spad = ArrayAdapter.createFromResource(getContext(), R.array.item, android.R.layout.simple_spinner_item);
        spad.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        viewbinding.spinner.setAdapter(spad);
        viewbinding.spinner.setOnItemSelectedListener(this);


        ProductRecycler();

        Search();

        btn_voice = view.findViewById(R.id.btn_voice);
        btn_voice.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                speak();
            }
        });

        return view;
    }

    // mic function
    private void speak() {
        Intent intent = new Intent(RecognizerIntent.ACTION_RECOGNIZE_SPEECH);
        intent.putExtra(RecognizerIntent.EXTRA_LANGUAGE_MODEL, RecognizerIntent.LANGUAGE_MODEL_FREE_FORM);
        intent.putExtra(RecognizerIntent.EXTRA_LANGUAGE, Locale.getDefault());
        intent.putExtra(RecognizerIntent.EXTRA_PROMPT, R.string.Speak_something);
        try {
            startActivityForResult(intent, REQUEST_CODE_SPEECH_INPUT);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    //mic function
    @Override
    public void onActivityResult(int requestCode, int resultCode, @Nullable Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (requestCode == REQUEST_CODE_SPEECH_INPUT) {
            if (resultCode == getActivity().RESULT_OK && null != data) {
                ArrayList<String> result = data.getStringArrayListExtra(RecognizerIntent.EXTRA_RESULTS);
                searchView.setQuery(result.get(0), false);
            }
        }
    }


    private void Search() {

        searchView.setOnQueryTextListener(new SearchView.OnQueryTextListener() {
            @Override
            public boolean onQueryTextSubmit(String s) {
                return false;
            }

            @Override
            public boolean onQueryTextChange(String s) {
                //call the filter function for user input productname
                if (adapter != null) {
                    adapter.performFiltering(new OnFilterListener<Product>() {
                        @Override
                        public List<Product> onFilter(List<Product> products) {
                            ArrayList<Product> filtered = new ArrayList<>();
                            if (s.isEmpty()) {
                                filtered.addAll(products);
                            } else {
                                for (Product row : products) {
                                    if (row.getProductName().toLowerCase().contains(s.toLowerCase())) {
                                        filtered.add(row);
                                    }
                                }
                            }
                            return filtered;
                        }
                    });
                }
                return true;
            }
        });
    }

    private void ProductRecycler() {
        productRecycler.setHasFixedSize(true);
        productRecycler.setLayoutManager(new LinearLayoutManager(getContext(), LinearLayoutManager.VERTICAL, false));

        //Call firebase
        reference = FirebaseDatabase.getInstance().getReference().child("Product");

        //Get the firebase data
        reference.addValueEventListener(new ValueEventListener() {
            @Override
            public void onDataChange(@NonNull DataSnapshot snapshot) {
                listproduct.clear();
                listproduct2.clear();
                for (DataSnapshot datas : snapshot.getChildren()) {
                    Product product = datas.getValue(Product.class);
                    product.setPid(datas.getKey());
                    listproduct.add(product);

                }

                String spinnertext = viewbinding.spinner.getSelectedItem().toString();

                //Select the product type
                if ("All".toLowerCase().matches(spinnertext.toLowerCase())) {
                    //set adapter
                    adapter = new ProductAdapter(listproduct, getContext());
                    productRecycler.setAdapter(adapter);
                } else {

                    for (Product object : listproduct) {
                        if (object.getProductType().toLowerCase().matches(spinnertext.toLowerCase())) {
                            listproduct2.add(object);

                        }
                    }
                    //set adapter
                    adapter = new ProductAdapter(listproduct2, getContext());
                    productRecycler.setAdapter(adapter);

                }


            }

            @Override
            public void onCancelled(@NonNull DatabaseError error) {

            }
        });

    }


    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
    }


    @Override
    public void onItemSelected(AdapterView<?> adapterView, View view, int i, long l) {
        //get the spinner item
        String spinnertext = adapterView.getItemAtPosition(i).toString();
        System.out.println(spinnertext);
        listproduct2.clear();

        //Select the product type

        if ("All".toLowerCase().matches(spinnertext.toLowerCase())) {
            adapter = new ProductAdapter(listproduct, getContext());
            productRecycler.setAdapter(adapter);

        } else {
            //Get the product type
            for (Product object : listproduct) {
                if (object.getProductType().toLowerCase().matches(spinnertext.toLowerCase())) {
                    listproduct2.add(object);
                }
            }
            adapter = new ProductAdapter(listproduct2, getContext());
            productRecycler.setAdapter(adapter);
        }


    }

    @Override
    public void onNothingSelected(AdapterView<?> adapterView) {

    }


}