package com.example.a300assignment_eshopnew.swipe;

import android.view.MotionEvent;

public interface OnActivityTouchListener {
    void getTouchCoordinates(MotionEvent ev);
}