package com.example.a300assignment_eshopnew.utils;

import android.os.Handler;
import android.os.Looper;

public class ThreadUtil {

    private static Handler handler = new Handler(Looper.getMainLooper());

    public static void post(Runnable runnable) {
        handler.post(runnable);
    }
}

